// Copyright 2017-2019 Apex.AI, Inc.
// Co-developed by Tier IV, Inc. and Apex.AI, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
/// \file
/// \brief This file includes common type definition

#ifndef COMMON__TYPES_HPP_
#define COMMON__TYPES_HPP_

namespace autoware
{
namespace common
{
namespace types
{
using float32_t = float;
using float64_t = double;
using bool8_t = bool;
}  // namespace types
}  // namespace common
}  // namespace autoware

#endif  // COMMON__TYPES_HPP_
